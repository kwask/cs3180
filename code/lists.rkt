#lang racket
; basic ops
; a list is a heterogenous group surrounded by parenthesis and seperated by spaces
; (1 2 3 4)
; ("a" "b" "c")
; ("a" 2 ("hello" "goodbye")) nested lists
(list 1 2 3 4) ; equivalent to '(1 2 3 4)

(car '(1 2 3 4)) ; returns first element
(cdr '(1 2 3 4)) ; returns new list after first element is removed
(cadr '(1 2 3 4)) ; gets 2nd element (returns new first element after first element is removed one time)
(caddr '(1 2 3 4)) ; gets 3rd element (returns new first element after first element is removed two times)
(cadddr '(1 2 3 4)) ; gets 4th element (returns new first element after first element is removed three times)
`
