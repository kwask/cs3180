/** ===========================================================================
 * author:          Nick Hirzel
 * email:           hirzel.2@wright.edu
 * date:            April 14, 2020
 *
 * course:          CS3180-01 Comparative Languages
 * assignment:      Homework 2
 * file:            Exprv.java
 * desc:            This program read a provided Java bytecode file and use the
 *                  provided variable values to produce a resulting value.
 *
 *                  Only the add, mul, load, and t2t codes are supported.
 *
 *                  Additional registers for variables can be added by adding to
 *                  registers map.
 * ========================================================================== */

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Exprv
{
    static Map<Integer, Variable> registers;
    static Deque<Number> stack;

    public static void main(String[] args) throws IOException
    {
        int max_args = 4;
        int min_args = 2;
        int ascii_number_start = 48;

        if(args.length < min_args || args.length > max_args)
        {
            System.out.println("Enter the bytecode filename, and up to 3 " +
                    "values for the variables.\n" +
                    "For example:expr.jbc 1 2.0 3");
            return;
        }

        registers = new HashMap<Integer, Variable>();
        registers.put(0, new Variable("i", 0));
        registers.put(1, new Variable("a", 1));
        registers.put(3, new Variable("j", 3));

        // the bytecode file is the first thing passed
        String code_file = args[0];
        System.out.println(code_file);

        // setting the values for the variables
        registers.get(0).setValue(Integer.parseInt(args[1]));
        System.out.println(registers.get(0));
        if(args.length >= 3)
        {
            registers.get(1).setValue(Double.parseDouble(args[2]));
            System.out.println(registers.get(1));
        }
        if(args.length == 4)
        {
            registers.get(3).setValue(Integer.parseInt(args[3]));
            System.out.println(registers.get(3));
        }

        // creating our stack and opening the bytecode file
        stack = new ArrayDeque<Number>();
        Scanner input = new Scanner(new File(code_file));

        try
        {
            while (input.hasNext())
            {
                // getting a code
                String line = input.nextLine();

                // breaking the code type off
                char type1 = line.charAt(0);
                String code = line.substring(1);

                if (code.equals("add")) // addition op
                {
                    // pop off top two values, add them, push the result
                    stack.push(addition(type1, stack.pop(), stack.pop()));
                    //System.out.printf("add: %s\n", stack.peek());
                } else if (code.equals("mul")) // multiply op
                {
                    // pop off top two values, multiply them, push the result
                    stack.push(multiply(type1, stack.pop(), stack.pop()));
                    //System.out.printf("mul: %s\n", stack.peek());
                } else if (code.charAt(0) == '2') // conversion op
                {
                    // pop off top value, convert to specified type, push result
                    char type2 = code.charAt(1);
                    pushValue(type2, stack.pop());
                    //System.out.printf("%c2%c: %s\n", type1, type2, stack.peek());
                } else  // load op
                {
                    // breaking register off of code
                    int reg = code.length() - 1;
                    int register = code.charAt(reg) - ascii_number_start;
                    String sub_code = code.substring(0, reg - 1);

                    // if the sub_code doesn't match load, we have no other
                    // codes that are supported
                    if (!sub_code.equals("load"))
                    {
                        throw new Exception(
                                String.format("Unsupported code \"%s\"",
                                        line));
                    }

                    // if the register was out of range
                    if (register < 0 || register > 3)
                    {
                        throw new Exception(
                                String.format("Illegal register \"%d\"",
                                        register));
                    }

                    // get value from register, push result as specified type
                    pushValue(type1, registers.get(register).getValue());
                    //System.out.printf("load: %s\n", stack.peek());
                }
            }

            // printing the final result on the stack
            System.out.printf("Final result: %s\n", stack.pop());
        }
        catch (Exception e)
        {
            System.out.printf("\tError => %s\n", e);
        }
    }

    // Takes two numbers, adds them, and returns the result
    static public Number addition(char type, Number value1, Number value2)
    {
        if(type == 'i')
        {
            return value1.intValue()+value2.intValue();
        }
        else if(type == 'd')
        {
            return value1.doubleValue()+value2.doubleValue();
        }

        return 0;
    }

    // Takes two numbers, multiplies them, and returns the result
    static public Number multiply(char type, Number value1, Number value2)
    {
        if(type == 'i')
        {
            return value1.intValue()*value2.intValue();
        }
        else if(type == 'd')
        {
            return value1.doubleValue()*value2.doubleValue();
        }

        return 0;
    }

    // Takes a number and a given type and pushes the value to the stack with
    //      that type
    static public void pushValue(char type, Number value)
    {
        if(type == 'd')
        {
            stack.push(value.doubleValue());
        }
        if(type == 'i')
        {
            stack.push(value.intValue());
        }
    }
}

// Variable object holding a specific value and pointing to a register
class Variable
{
    private String id;
    private int register;
    private Number value;

    public Variable(String _id, int _register)
    {
        id = _id;
        register = _register;
    }

    public String getId()
    {
        return id;
    }

    public int getRegister()
    {
        return register;
    }

    public Number getValue()
    {
        return value;
    }

    public void setValue(Number value) { this.value = value; }

    @Override
    public String toString()
    {
        return "(" + register + "): " + id + " = " + value;
    }
}
