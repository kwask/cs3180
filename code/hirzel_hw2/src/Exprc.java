/** ===========================================================================
 * author:          Nick Hirzel
 * email:           hirzel.2@wright.edu
 * date:            April 14, 2020
 *
 * course:          CS3180-01 Comparative Languages
 * assignment:      Homework 2
 * file:            Exprc.java
 * desc:            This program will read the mathematical expressions in the
 *                  file "expr.dat" and translate them into corresponding Java
 *                  bytecode.
 *
 *                  Only the '+', '*', '(', and ')' operators are supported.
 *
 *                  Variables 'i' (int), 'j' (double), and 'a' (int) are
 *                  available.
 *
 *                  More variables may be added by adding to the variables map.
 * ========================================================================== */

import java.io.*;
import java.util.StringTokenizer;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class Exprc
{
    static BufferedReader in;
    static StreamTokenizer tokenizer;
    static Map<String, Register> variables;
    static ArrayList<Character> operators;

    public static void main(String[] args) throws IOException
    {
        // setting up table of variable names, their registers, and their types
        variables = new HashMap<String, Register>();
        variables.put("i", new Register(0, 'i'));
        variables.put("a", new Register(1, 'd'));
        variables.put("j", new Register(3, 'i'));

        // legal operators
        operators = new ArrayList<Character>();
        operators.add('+');
        operators.add('*');
        operators.add('(');
        operators.add(')');

        // setting input and output filenames
        String input_filename = "expr.dat";
        String output_filename = input_filename.substring(
                0,
                input_filename.indexOf("."));
        output_filename += ".jbc"; // java bytecode

        in = new BufferedReader(new FileReader(input_filename));

        // processes a sequence of expressions to java bytecode
        // retains the translation of the last one in output_filename
        String line = in.readLine();
        while (line != null && line.length() > 0)
        {
            // preprocess line to add whitespace between all symbols
            String preproc_line = preprocess(line);
            System.out.println(preproc_line);
            Reader preproc = new StringReader(preproc_line);

            // set up tokenizer for expression on this line
            tokenizer = new StreamTokenizer(preproc);
            tokenizer.nextToken();

            if (tokenizer.ttype != tokenizer.TT_EOL)
            {
                PrintWriter out = new PrintWriter(
                        new FileWriter(output_filename),
                        true);
                try
                {
                    Node root = expr();
                    System.out.println(" \tType => " + root.type());
                    root.code(out);
                    out.flush();
                } catch (Exception e)
                {
                    System.out.println(" \tError =>  " + e);
                } finally
                {
                    out.close();
                }
            }
            preproc.close();
            line = in.readLine();
        }
    }

    public static String preprocess(String line)
    {
        StringTokenizer preproc = new StringTokenizer(line);
        String output = "";

        while (preproc.hasMoreTokens())
        {
            String token = preproc.nextToken();
            if(output.length() > 0 && output.charAt(output.length()-1) != ' ')
            {
                output += ' ';
            }

            String parsed = "";
            // surrounds operators with whitespace
            for(int i = 0; i < token.length(); i++)
            {
                char ch = token.charAt(i);

                if (operators.contains(ch))
                {
                    if(parsed.length() > 0
                    && parsed.charAt(parsed.length()-1) != ' ')
                    {
                        parsed += ' ';
                    }
                    parsed += ch;
                    parsed += ' ';
                }
                else
                {
                    parsed += ch;
                }
            }

            output += parsed; // maintain whitespace between tokens
        }

        return output;
    }

    // First grammar rule: <expr> ->  <term> { + <term> }
    // This ensures addition has the lowest precedence
    // This addition rule is left-associative
    public static Node expr() throws Exception
    {
        Node root = term();
        while(tokenizer.ttype == '+')
        {
            tokenizer.nextToken();
            root = new AdditionNode(root, term());
        }
        return root;
    }

    // Second grammar rule: <term> -> <factor> { * <factor> }
    // This ensures multiplication has higher precedence than addition
    // This multiplication rule is left-associative
    public static Node term() throws Exception
    {
        Node root = factor();
        while(tokenizer.ttype == '*')
        {
            tokenizer.nextToken();
            root = new MultiplyNode(root, factor());
        }
        return root;
    }

    // Third grammar rule: <factor> -> a | i | j | ( <expr> )
    // Either we end at a terminal (a variable) or we start a new expression
    //      that was in parenthesis
    public static Node factor() throws Exception
    {
        Node temp;
        if (tokenizer.ttype == tokenizer.TT_WORD)
        {
            String id = tokenizer.sval;
            if (variables.containsKey(id))
            {
                temp = new VarNode(variables.get(id));
                tokenizer.nextToken();
            } else throw new Exception("Illegal variable name");
        }
        else if (tokenizer.ttype == '(')
        {
            // move past '('
            tokenizer.nextToken();
            // begin a new sub expression
            temp = expr();
            // check for ')' and move past it
            if(tokenizer.ttype != ')')
            {
                throw new Exception("Unclosed parenthesis");
            }
            tokenizer.nextToken();
        }
        else throw new Exception("Variable token or parenthesis expected");
        return temp;
    }
}

// Base node pure abstract class
interface Node
{
    // The type underlying this node
    char type();

    // The bytecode that this node will resolve to
    String byteCode();

    // This will output the bytecode representing this node to out
    void code(PrintWriter out);
}

// Terminal node to hold variables
class VarNode implements Node
{
    Register reg;

    public VarNode(Register _reg)
    {
        this.reg = _reg;
    }

    public char type()
    {
        return reg.getType();
    }

    public String byteCode()
    {
        return String.format("%cload_%d", this.type(), this.reg.getRegister());
    }

    public void code(PrintWriter out)
    {
        out.printf("%s\n", this.byteCode());
    }
}

// Abstract node for operators
abstract class OpNode implements Node
{
    private Node left, right;

    public OpNode(Node _LNode, Node _RNode)
    {
        left = _LNode;
        right = _RNode;
    }

    public char type()
    {
        if(left.type() != right.type())
        {
            // since we only handle ints or doubles, any mismatch
            // needs to be a double
            return 'd';
        }
        else
        {
            return left.type();
        }
    }

    public abstract String byteCode();

    public void code(PrintWriter out)
    {
        left.code(out);
        if(left.type() == 'i' && right.type() == 'd')
        {
            out.printf("i2d\n");
        }

        right.code(out);
        if(left.type() == 'd' && right.type() == 'i')
        {
            out.printf("i2d\n");
        }
        out.printf("%s\n", this.byteCode());
    }

    public void setLeft(Node left)
    {
        this.left = left;
    }

    public void setRight(Node right)
    {
        this.right = right;
    }

    public Node getLeft()
    {
        return left;
    }

    public Node getRight()
    {
        return right;
    }
}

// Node for addition operator
class AdditionNode extends OpNode
{
    public AdditionNode(Node _LNode, Node _RNode)
    {
        super(_LNode, _RNode);
    }

    public String byteCode()
    {
        return String.format("%cadd", this.type());
    }
}

// Node for multiplication operator
class MultiplyNode extends OpNode
{
    public MultiplyNode(Node _LNode, Node _RNode)
    {
        super(_LNode, _RNode);
    }

    public String byteCode()
    {
        return String.format("%cmul", this.type());
    }
}

// Object for a register which a variable is using
// Includes the register id and type
class Register
{
    int register; // 0, 1, or 3 for the purposes of this program
    char type; // either 'i' or 'd'

    Register(int _register, char _type)
    {
        this.register = _register;
        this.type = _type;
    }

    public int getRegister()
    {
        return register;
    }

    public char getType()
    {
        return type;
    }
}
